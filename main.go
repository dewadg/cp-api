package main

import (
	"context"
	"fmt"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"gitlab.com/dewadg/cp-api/cmd/dbmigrate"
	"gitlab.com/dewadg/cp-api/cmd/keygen"
	"gitlab.com/dewadg/cp-api/cmd/serve"
	"gitlab.com/dewadg/cp-api/internal/keys"
)

const version = "0.1.0-alpha"

var rootCmd = &cobra.Command{
	Use: "cp",
}

func init() {
	godotenv.Load()
}

func main() {
	rootCmd.AddCommand(dbmigrate.Command())
	rootCmd.AddCommand(serve.Command())
	rootCmd.AddCommand(keygen.Command())

	ctx := context.Background()
	ctx = context.WithValue(ctx, keys.KeyAppVersion, version)

	if err := rootCmd.ExecuteContext(ctx); err != nil {
		fmt.Println(err)
		return
	}
}
