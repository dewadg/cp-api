package models

import "time"

type Bin struct {
	ID        uint64     `json:"-"`
	Slug      string     `gorm:"index" json:"slug"`
	Content   string     `json:"content"`
	CreatedAt time.Time  `gorm:"index" json:"-"`
	DeletedAt *time.Time `gorm:"index" json:"-"`
}
