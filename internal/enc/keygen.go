package enc

import (
	"encoding/hex"
	"math/rand"
)

func GenerateKey() (string, error) {
	keyBytes := make([]byte, 32)
	if _, err := rand.Read(keyBytes); err != nil {
		return "", err
	}

	return hex.EncodeToString(keyBytes), nil
}
