package services

import (
	"context"
	"errors"
	"log"
	"math/rand"
	"time"

	"gitlab.com/dewadg/cp-api/internal/enc"
	"gitlab.com/dewadg/cp-api/internal/entities/models"
	"gitlab.com/dewadg/cp-api/internal/errs"
	"gitlab.com/dewadg/cp-api/internal/repositories"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")

type binService struct {
	encKey          string
	mysqlBinRepo    repositories.BinRepository
	inmemoryBinRepo repositories.BinRepository
}

func NewBinService(encKey string, mysqlBinRepo repositories.BinRepository, inmemoryBinRepo repositories.BinRepository) BinService {
	return &binService{
		encKey:          encKey,
		mysqlBinRepo:    mysqlBinRepo,
		inmemoryBinRepo: inmemoryBinRepo,
	}
}

func (svc *binService) Create(ctx context.Context, data models.Bin) (models.Bin, error) {
	data.Slug = svc.generateSlug()
	for {
		duplicateBin, _ := svc.FindBySlug(ctx, data.Slug)
		if duplicateBin.ID == 0 {
			break
		}
	}

	encryptedContent, err := enc.Encrypt(svc.encKey, data.Content)
	if err != nil {
		return models.Bin{}, err
	}
	data.Content = encryptedContent

	newBin, err := svc.mysqlBinRepo.Create(ctx, data)
	if err != nil {
		return models.Bin{}, err
	}

	decryptedContent, err := enc.Decrypt(svc.encKey, newBin.Content)
	if err != nil {
		log.Println("Error while storing bin to in-memory cache:", err.Error())
	}
	newBin.Content = decryptedContent

	go func() {
		if _, err = svc.inmemoryBinRepo.Create(ctx, newBin); err != nil {
			log.Println("Error while storing bin to in-memory cache:", err.Error())
		}
	}()

	return newBin, nil
}

func (svc *binService) generateSlug() string {
	b := make([]rune, 5)

	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	return string(b)
}

func (svc *binService) FindBySlug(ctx context.Context, slug string) (models.Bin, error) {
	bin, err := svc.inmemoryBinRepo.FindBySlug(ctx, slug)
	if err != nil {
		if errors.Is(err, errs.ErrBinNotFound) {
			bin, err = svc.mysqlBinRepo.FindBySlug(ctx, slug)
			if err != nil {
				return models.Bin{}, err
			}

			decryptedContent, err := enc.Decrypt(svc.encKey, bin.Content)
			if err != nil {
				return models.Bin{}, err
			}
			bin.Content = decryptedContent

			go func() {
				if _, err := svc.inmemoryBinRepo.Create(ctx, bin); err != nil {
					log.Println("Error while storing bin to in-memory cache:", err.Error())
				}
			}()

			return bin, nil
		}
	}

	return bin, err
}

func (svc *binService) DeleteOutdated(ctx context.Context) ([]string, error) {
	desiredTime := time.Now().Add(-15 * time.Minute)

	outdatedBins, err := svc.mysqlBinRepo.GetBefore(ctx, desiredTime)
	if err != nil {
		return nil, err
	}
	if len(outdatedBins) == 0 {
		return nil, nil
	}

	outdatedSlugs := make([]string, len(outdatedBins))
	for i := range outdatedBins {
		outdatedSlugs[i] = outdatedBins[i].Slug
	}

	if _, err := outdatedSlugs, svc.mysqlBinRepo.DeleteBySlugs(ctx, outdatedSlugs); err != nil {
		return nil, err
	}

	return outdatedSlugs, svc.inmemoryBinRepo.DeleteBySlugs(ctx, outdatedSlugs)
}
