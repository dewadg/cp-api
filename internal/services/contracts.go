package services

import (
	"context"
	"gitlab.com/dewadg/cp-api/internal/entities/models"
)

type BinService interface {
	Create(ctx context.Context, data models.Bin) (models.Bin, error)

	FindBySlug(ctx context.Context, slug string) (models.Bin, error)

	DeleteOutdated(ctx context.Context) ([]string, error)
}
