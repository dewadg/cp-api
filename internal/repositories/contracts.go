package repositories

import (
	"context"
	"time"

	"gitlab.com/dewadg/cp-api/internal/entities/models"
)

type BinRepository interface {
	Create(ctx context.Context, data models.Bin) (models.Bin, error)

	FindBySlug(ctx context.Context, slug string) (models.Bin, error)

	DeleteBySlugs(ctx context.Context, slugs []string) error

	GetBefore(ctx context.Context, t time.Time) ([]models.Bin, error)
}
