package repositories

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/dewadg/cp-api/internal/entities/models"
	"gitlab.com/dewadg/cp-api/internal/errs"
	"gorm.io/gorm"
)

type mysqlBinRepository struct {
	db *gorm.DB
}

func NewMysqlBinRepository(db *gorm.DB) BinRepository {
	return &mysqlBinRepository{
		db: db,
	}
}

func (repo *mysqlBinRepository) Create(ctx context.Context, data models.Bin) (models.Bin, error) {
	data.CreatedAt = time.Now().UTC()

	err := repo.db.Create(&data).Error

	return data, err
}

func (repo *mysqlBinRepository) FindBySlug(ctx context.Context, slug string) (models.Bin, error) {
	var bin models.Bin

	err := repo.db.
		Where("slug = ?", slug).
		First(&bin).
		Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			err = fmt.Errorf("%s: %w", slug, errs.ErrBinNotFound)
		}
		return models.Bin{}, err
	}

	return bin, nil
}

func (repo *mysqlBinRepository) DeleteBySlugs(ctx context.Context, slugs []string) error {
	return repo.db.Delete(models.Bin{}, "slug IN (?)", slugs).Error
}

func (repo *mysqlBinRepository) GetBefore(ctx context.Context, t time.Time) ([]models.Bin, error) {
	var bins []models.Bin

	err := repo.db.
		Where("created_at < ?", t).
		Find(&bins).
		Error

	return bins, err
}
