package repositories

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/dewadg/cp-api/internal/entities/models"
	"gitlab.com/dewadg/cp-api/internal/errs"
)

type inmemoryBinRepository struct {
	binMap map[string]models.Bin
	lock   sync.Mutex
}

func NewInmemoryBinRepository() BinRepository {
	return &inmemoryBinRepository{
		binMap: make(map[string]models.Bin),
	}
}

func (repo *inmemoryBinRepository) Create(ctx context.Context, data models.Bin) (models.Bin, error) {
	repo.lock.Lock()
	defer repo.lock.Unlock()

	repo.binMap[data.Slug] = data

	return data, nil
}

func (repo *inmemoryBinRepository) FindBySlug(ctx context.Context, slug string) (models.Bin, error) {
	repo.lock.Lock()
	defer repo.lock.Unlock()

	bin, found := repo.binMap[slug]
	if !found {
		return models.Bin{}, fmt.Errorf("%s: %w", slug, errs.ErrBinNotFound)
	}

	return bin, nil
}

func (repo *inmemoryBinRepository) DeleteBySlugs(ctx context.Context, slugs []string) error {
	for i := range slugs {
		if _, found := repo.binMap[slugs[i]]; found {
			delete(repo.binMap, slugs[i])
		}
	}

	return nil
}

func (repo *inmemoryBinRepository) GetBefore(ctx context.Context, t time.Time) ([]models.Bin, error) {
	bins := make([]models.Bin, 0)

	for _, bin := range repo.binMap {
		if bin.CreatedAt.Before(t) {
			bins = append(bins, bin)
		}
	}

	return bins, nil
}
