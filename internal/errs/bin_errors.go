package errs

import "errors"

var ErrBinNotFound = errors.New("bin_not_found")
