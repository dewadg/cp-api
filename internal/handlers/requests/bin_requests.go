package requests

import (
	"errors"
	"net/http"
)

type CreateBinRequest struct {
	Content string `json:"content"`
}

func (req CreateBinRequest) Bind(request *http.Request) error {
	if req.Content == "" {
		return errors.New("`content` is required")
	}
	return nil
}
