package responses

import (
	"net/http"

	"gitlab.com/dewadg/cp-api/internal/entities/models"
)

type BinResponse struct {
	Data models.Bin `json:"data"`
}

func (resp BinResponse) Render(writer http.ResponseWriter, request *http.Request) error {
	return nil
}
