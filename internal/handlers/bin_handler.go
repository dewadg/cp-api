package handlers

import (
	"errors"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"gitlab.com/dewadg/cp-api/internal/entities/models"
	"gitlab.com/dewadg/cp-api/internal/errs"
	"gitlab.com/dewadg/cp-api/internal/handlers/requests"
	"gitlab.com/dewadg/cp-api/internal/handlers/responses"
	"gitlab.com/dewadg/cp-api/internal/services"
)

type binHandler struct {
	binSvc services.BinService
}

func NewBinHandler(binSvc services.BinService) Handler {
	return &binHandler{
		binSvc: binSvc,
	}
}

func (h *binHandler) Register(router chi.Router, prefix string) {
	subRouter := chi.NewRouter()

	subRouter.Post("/", h.create)
	subRouter.Get("/{slug}", h.findBySlug)

	router.Mount(prefix, subRouter)
}

func (h *binHandler) create(writer http.ResponseWriter, request *http.Request) {
	var requestData requests.CreateBinRequest
	if err := render.Bind(request, &requestData); err != nil {
		http.Error(writer, err.Error(), http.StatusUnprocessableEntity)
		return
	}

	newBin, err := h.binSvc.Create(request.Context(), models.Bin{
		Content: requestData.Content,
	})
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	response := responses.BinResponse{
		Data: newBin,
	}
	render.Render(writer, request, response)
}

func (h *binHandler) findBySlug(writer http.ResponseWriter, request *http.Request) {
	bin, err := h.binSvc.FindBySlug(request.Context(), chi.URLParam(request, "slug"))
	if err != nil {
		status := http.StatusInternalServerError
		if errors.Is(err, errs.ErrBinNotFound) {
			status = http.StatusNotFound
		}

		http.Error(writer, err.Error(), status)
		return
	}

	response := responses.BinResponse{
		Data: bin,
	}
	render.Render(writer, request, response)
}
