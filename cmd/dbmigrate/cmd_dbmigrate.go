package dbmigrate

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/dewadg/cp-api/internal/entities/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Command() *cobra.Command {
	return &cobra.Command{
		Use: "db:migrate",
		Run: func(cmd *cobra.Command, args []string) {
			db, err := gorm.Open(mysql.Open(os.Getenv("MYSQL_DSN")), &gorm.Config{})
			if err != nil {
				fmt.Println(err)
				return
			}

			err = db.AutoMigrate(
				models.Bin{},
			)

			if err != nil {
				fmt.Println(err)
				return
			}
		},
	}
}
