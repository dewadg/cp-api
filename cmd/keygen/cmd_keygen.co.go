package keygen

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/dewadg/cp-api/internal/enc"
)

func Command() *cobra.Command {
	return &cobra.Command{
		Use: "keygen",
		Run: func(cmd *cobra.Command, args []string) {
			key, err := enc.GenerateKey()
			if err != nil {
				fmt.Println(err)
				return
			}

			fmt.Println(key)
		},
	}
}
