package serve

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	"github.com/spf13/cobra"
	"gitlab.com/dewadg/cp-api/internal/handlers"
	"gitlab.com/dewadg/cp-api/internal/keys"
	"gitlab.com/dewadg/cp-api/internal/repositories"
	"gitlab.com/dewadg/cp-api/internal/services"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Command() *cobra.Command {
	return &cobra.Command{
		Use: "serve",
		Run: func(cmd *cobra.Command, args []string) {
			db, err := gorm.Open(mysql.Open(os.Getenv("MYSQL_DSN")), &gorm.Config{})
			if err != nil {
				fmt.Println(err)
				return
			}

			mysqlBinRepo := repositories.NewMysqlBinRepository(db)
			inmemoryBinRepo := repositories.NewInmemoryBinRepository()
			binSvc := services.NewBinService(os.Getenv("ENCRYPTION_KEY"), mysqlBinRepo, inmemoryBinRepo)
			binHandler := handlers.NewBinHandler(binSvc)

			router := chi.NewRouter()
			router.Use(middleware.RequestID)
			router.Use(middleware.RealIP)
			router.Use(middleware.Logger)
			router.Use(middleware.Recoverer)
			router.Use(render.SetContentType(render.ContentTypeJSON))
			router.Use(cors.Handler(cors.Options{
				AllowedOrigins:   []string{"*"},
				AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
				AllowedHeaders:   []string{"Accept", "Content-Type"},
				AllowCredentials: false,
			}))
			router.Get("/", func(writer http.ResponseWriter, request *http.Request) {
				fmt.Fprint(writer, fmt.Sprintf("cp API v%s", cmd.Context().Value(keys.KeyAppVersion).(string)))
			})

			binHandler.Register(router, "/v1/bins")

			host := "localhost"
			if os.Getenv("APP_ENV") != "local" {
				host = "0.0.0.0"
			}
			port := "8000"
			if os.Getenv("APP_PORT") != "" {
				port = os.Getenv("APP_PORT")
			}
			address := fmt.Sprintf("%s:%s", host, port)

			go func() {
				runPeriodicDeletion(cmd.Context(), binSvc)
			}()

			fmt.Println("Running on", address)
			if err := http.ListenAndServe(address, router); err != nil {
				fmt.Println(err)
				return
			}
		},
	}
}

func runPeriodicDeletion(ctx context.Context, binSvc services.BinService) {
	ticker := time.NewTicker(5 * time.Minute)

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			log.Println("Running periodic deletion")

			deletedSlugs, err := binSvc.DeleteOutdated(ctx)
			if err != nil {
				fmt.Println("Error,", err.Error())
				continue
			}

			log.Println("Bins deleted:", strings.Join(deletedSlugs, ", "))
		}
	}
}
