module gitlab.com/dewadg/cp-api

go 1.15

require (
	github.com/go-chi/chi v1.5.0
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/joho/godotenv v1.3.0
	github.com/spf13/cobra v1.1.1
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
